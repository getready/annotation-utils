# annotation-utils
#### 介绍

这个工具功能包括

1、提取java类和方法以及参数的注解

2、给java类和方法添加注解

3、提取java类中的字段

#### 使用说明

maven引入：

```xml
<dependency>
  <groupId>io.github.ronghuaxueleng</groupId>
  <artifactId>annotation-utils</artifactId>
  <version>${lastVersion}</version>
</dependency>
```

